resource "aws_security_group" "security_group" {
  name = "${var.aws_deploy_security_group_name}"
}

resource "aws_security_group_rule" "postgres_instance_ssh" {
  security_group_id = aws_security_group.security_group.id

  type              = "ingress"
  from_port         = 22
  to_port           = 22
  protocol          = "tcp"
  cidr_blocks       = "${var.aws_deploy_allow_ssh_from_cidrs}"
}

resource "aws_security_group_rule" "postgres_netdata_local" {
  security_group_id         = aws_security_group.security_group.id
  type                      = "ingress"
  from_port                 = 19999
  to_port                   = 19999
  protocol                  = "tcp"
  cidr_blocks               = ["0.0.0.0/0"]
}

resource "aws_security_group_rule" "postgres_port" {
  security_group_id = aws_security_group.security_group.id
  type              = "ingress"
  from_port         = "${var.postgres_port}"
  to_port           = "${var.postgres_port}"
  protocol          = "tcp"
  cidr_blocks       = "${var.aws_deploy_allow_ssh_from_cidrs}"
}

resource "aws_security_group_rule" "postgres_instance_egress" {
  security_group_id = aws_security_group.security_group.id
  type              = "egress"
  from_port         = 0
  to_port           = 0
  protocol          = "-1"
  cidr_blocks       = ["0.0.0.0/0"]
}
