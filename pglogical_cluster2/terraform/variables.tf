variable "aws_ami_name" {
  description = "Filter to find an AMI"
  default = "*focal*"
}
variable "aws_deploy_region" {
  description = "AWS Region"
  default = "us-east-1"
}
variable "aws_deploy_availability_zone" {
   description = "AWS availability zone"
   default = "us-east-1a"
}
variable "aws_ami_owner" {
  description = "Filter for the AMI owner"
  default = "099720109477" # ubuntu AMI owner
}
variable "aws_deploy_ec2_instance_type" {
  description = "Type of EC2 instance"
  default = "c5.large"
}
variable "aws_spot_price" {
  description = "Price for spot request"
  default = "0.1"
}

variable "aws_deploy_root_ebs_size"{
  description = "Size of the root device of VM"
  default = "40"
}
variable "aws_deploy_allow_ssh_from_cidrs" {
   description = "List of CIDRs allowed to connect to SSH"
   default = ["0.0.0.0/0"]
}
variable "aws_deploy_ec2_provider_instance_tag_name" {
  description = "EC2 instance for provider"
  default = "pglogical-test-provider"
}
variable "aws_deploy_ec2_provider_instance_tag_ansible" {
  description = "Ansible group for provider"
  default = "pg_provider"
}
variable "aws_deploy_ec2_provider2_instance_tag_name" {
  description = "EC2 instance for provider2"
  default = "pglogical-test-provider2"
}
variable "aws_deploy_ec2_provider2_instance_tag_ansible" {
  description = "Ansible group for provider2"
  default = "pg_provider2"
}
variable "aws_deploy_ec2_subscriber_instance_tag_name" {
  description = "EC2 instance for subscriber"
  default = "pglogical-test-subscriber"
}
variable "aws_deploy_ec2_subscriber_instance_tag_ansible" {
  description = "Ansible group for subscriber"
  default = "pg_subscriber"
}
variable "aws_deploy_ec2_key_name" {
  description = "Key pair name used for EC2 auth"
  default = "pg_logical_test"
}

variable "aws_deploy_security_group_name" {
  description = "Security group name"
  default = "pg_logical_test"
}

variable "postgres_port" {
  description = "Postgresql listen port"
  default = "6666"
}

variable "postgres_version" {
  description = "Postgresql version"
  default = "13"
}
