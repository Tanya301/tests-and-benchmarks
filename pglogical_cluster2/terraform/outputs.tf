output "postgres_provider_ec2_ip" {
  value = "${aws_spot_instance_request.postgres_ec2_provider.public_ip}"
}
output "postgres_provider2_ec2_ip" {
  value = "${aws_spot_instance_request.postgres_ec2_provider2.public_ip}"
}
output "postgres_subscriber_ec2_ip" {
  value = "${aws_spot_instance_request.postgres_ec2_subscriber.public_ip}"
}
resource "local_file" "ansible_inventory" {
  content = templatefile("inventory.tpl",
    {
     pg_version = "${var.postgres_version}",
     pg_port = "${var.postgres_port}",
     pg_provider_ip = "${aws_spot_instance_request.postgres_ec2_provider.public_ip}",
     pg_provider2_ip = "${aws_spot_instance_request.postgres_ec2_provider2.public_ip}",
     pg_subscriber_ip = "${aws_spot_instance_request.postgres_ec2_subscriber.public_ip}",
     key_name = "${var.aws_deploy_ec2_key_name}.pem"
    }
  )
  filename = "../inventory.txt"
}
