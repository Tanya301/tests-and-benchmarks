[all:vars]
pg_version=${pg_version}
pg_provider=${pg_provider_ip}
pg_provider2=${pg_provider2_ip}
pg_subscriber=${pg_subscriber_ip}
pg_port=${pg_port}
pg_appaddr=3.0.0.121
pg_db_user=pglogical
pg_db_name=pglogical
pg_password=welcome1
ansible_ssh_private_key_file=../${key_name}
ansible_ssh_common_args='-o StrictHostKeyChecking=no'

[pg_provider]
${pg_provider_ip}
#[pg_provider:vars]

[pg_provider2]
${pg_provider2_ip}
#[pg_provider:vars]

[pg_subscriber]
${pg_subscriber_ip}
#[pg_subscriber:vars]

