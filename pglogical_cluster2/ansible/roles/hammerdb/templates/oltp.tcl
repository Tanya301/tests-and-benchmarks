puts "SETTING CONFIGURATION"

dbset db pg
diset connection pg_host {{ pg_provider }}
diset connection pg_port {{ pg_port }}
diset tpcc pg_count_ware 10
diset tpcc pg_num_vu 10
diset tpcc pg_partition false
diset tpcc pg_superuser postgres

diset tpcc pg_superuserpass {{ pg_password }}
diset tpcc pg_defaultdbase {{ pg_db_name }}
diset tpcc pg_user postgres
diset tpcc pg_pass {{ pg_password }}
diset tpcc pg_dbase {{ pg_db_name }}

print dict
buildschema
waittocomplete

puts "SCHEMA READY"
