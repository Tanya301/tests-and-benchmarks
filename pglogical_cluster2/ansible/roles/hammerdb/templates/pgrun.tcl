#!/bin/tclsh

puts "SETTING CONFIGURATION"

dbset db pg
diset connection pg_host {{ pg_provider  }}
diset connection pg_port {{ pg_port  }}
diset tpcc pg_count_ware 500
diset tpcc pg_num_vu 50
diset tpcc pg_partition false
diset tpcc pg_superuser postgres

diset tpcc pg_superuserpass {{ pg_password }}
diset tpcc pg_defaultdbase {{ pg_db_name }}
diset tpcc pg_user postgres
diset tpcc pg_pass {{ pg_password }}
diset tpcc pg_dbase {{ pg_db_name }}

diset tpcc pg_driver timed
diset tpcc pg_rampup 2
diset tpcc pg_duration 20
diset tpcc pg_vacuum true

loadscript
puts "SEQUENCE STARTED"
vuset logtotemp 1
vuset vu 50
vucreate
vurun
runtimer 1500
vudestroy
after 1500
puts "TEST SEQUENCE COMPLETE"
exit
