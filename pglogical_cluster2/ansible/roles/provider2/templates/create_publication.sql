SELECT pglogical.create_node(
    node_name := 'provider2',
    dsn := 'host=pg-provider2 port={{ pg_port }} dbname={{ pg_db_name }} user=postgres password={{ pg_password }}'
);


Create or replace function random_string(length integer) returns text as
$$
declare
  chars text[] := '{0,1,2,3,4,5,6,7,8,9,A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z,a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z}';
  result text := '';
  i integer := 0;
begin
  if length < 0 then
    raise exception 'Given length cannot be less than 0';
  end if;
  for i in 1..length loop
    result := result || chars[1+random()*(array_length(chars, 1)-1)];
  end loop;
  return result;
end;
$$ language plpgsql;


create table second_provider_test (
    id             serial,
    username       text not null,
    password       text,
    created_on     timestamptz not null,
    last_logged_on timestamptz not null,
    CONSTRAINT prt_test_pk PRIMARY KEY (id, created_on)
);

SELECT pglogical.create_replication_set(
    set_name := 'second_provider_repl_set',
    replicate_insert := TRUE, replicate_update := TRUE,
    replicate_delete := TRUE, replicate_truncate := TRUE
);

SELECT pglogical.replication_set_add_table(
    set_name := 'second_provider_repl_set', relation := 'second_provider_test', 
    synchronize_data := TRUE
);


