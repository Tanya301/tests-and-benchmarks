#!/bin/bash

set -e -o pipefail

# 1. Reset PostgreSQL statistics before running tests
# 2. Execute pgbench tests
# 3. Collect PostgreSQL artifacts

# Examples:

# workload_pgbench only
#./test_with_artifacts.sh '{
#  "w/o pgbouncer": {
#    "workload_pgbench": "pgbench -U postgres -c4 -j4 -nr -P10 -T60 postgres -p5432 -hlocalhost"
#  },
#  "with pgbouncer": {
#    "workload_pgbench": "pgbench -U postgres -c4 -j4 -nr -P10 -T60 postgres -p6432 -hlocalhost"
#  }
#}'

# pre_sql and workload_pgbench
#./test_with_artifacts.sh '{
#  "1 index": {
#    "pre_sql": "create index if not exists idx_pgbench_accounts_aid on pgbench_accounts(aid)",
#    "workload_pgbench": "pgbench -U postgres -c 4 -j 4 -T 60 -P 10 postgres"
#  },
#  "2 indexes": {
#    "pre_sql": "create index if not exists idx_pgbench_accounts_bid on pgbench_accounts(bid)",
#    "workload_pgbench": "pgbench -U postgres -c 4 -j 4 -T 60 -P 10 postgres"
#  }
#}'

# pre_configs and workload_pgbench
#./test_with_artifacts.sh '{
#  "wal_compression OFF": {
#    "pre_configs": [
#      "wal_compression=off",
#      "work_mem=100MB"
#    ],
#    "workload_pgbench": "pgbench -U postgres -c4 -j4 -nr -P10 -T60 postgres"
#  },
#  "wal_compression ON": {
#    "pre_configs": [
#      "wal_compression=on",
#      "work_mem=100MB"
#    ],
#    "workload_pgbench": "pgbench -U postgres -c4 -j4 -nr -P10 -T60 postgres"
#  }
#}'

ARTIFACTS_DIR="${ARTIFACTS_DIR:-/tmp/ARTIFACTS}"
# create artifacts directory (if not exist)
mkdir -p "${ARTIFACTS_DIR}"

SOURCE_HOST="${SOURCE_HOST:-localhost}"
SOURCE_PORT="${SOURCE_PORT:-5432}"
SOURCE_USER="${SOURCE_USER:-postgres}"
SOURCE_DB="${SOURCE_DB:-postgres}"

# Clear file system cache (default 'false')
CLEAR_FS_CACHE="${CLEAR_FS_CACHE:-false}"

# Postgres log directory path
PG_LOG_DIR="${PG_LOG_DIR:-""}"

if [[ -z "${PGPASSWORD}" ]] && [[ "${CHECK_ONLY}" != "true" ]]; then
  export PGPASSWORD=$(grep -A2 "superuser:" /etc/patroni/patroni.yml | grep password | awk '{print $2}')
fi

function prefixes() {
  ts '%Y-%m-%d %H:%M:%S' | ts "[${label}|${type}]"
}

function info() {
  local msg="$1"
  echo -e "INFO: ${msg}" | prefixes >&1
}

function error() {
  # Print error message and exit
  local msg="$1"
  echo -e "ERROR: ${msg}" | prefixes >&2

  if [[ -z "${CHECK_ONLY}" ]] || [[ "${CHECK_ONLY}" != "true" ]]; then
    # Stop observation (if it is running)
    stop_sar_observation
    stop_pidstat_observation

    # Collect artifacts
    if [[ "${ARTIFACTS_COLLECTED}" != true ]]; then
      collect_artifacts "${safe_label}"
      tar -zcvf "${ARTIFACTS_DIR}".tar.gz "${ARTIFACTS_DIR}" &>/dev/null
    fi
  fi

  # stop the script with the return code 1
  exit 1
}

function _PSQL() {
  psql -h "${SOURCE_HOST}" -p "${SOURCE_PORT}" -d "${SOURCE_DB}" -U "${SOURCE_USER}" "$@"
}

function reset_pg_stat() {
  info "Resetting various types of statistics in Postgres, including extensions..."

  # Reset various stats at once
  # Recipe from: https://gitlab.com/postgres-ai/postgresql-consulting/postgres-howtos/-/blob/main/0050_pre_and_post_steps_for_benchmark_iterations.md#pre-step-reset-statistics
  if ! _PSQL -tAX <<EOF
    do \$\$
    declare
      -- Main reset commands – a JSON object having the form:
      --                      {"command": min-PG-version-num}
      -- todo: in PG17+, there is pg_stat_reset_shared(null)
      reset_cmd_main json := \$json\${
        "pg_stat_reset()":                             90000,
        "pg_stat_reset_shared('bgwriter')":            90000,
        "pg_stat_reset_shared('archiver')":            90000,
        "pg_stat_reset_shared('io')":                 160000,
        "pg_stat_reset_shared('wal')":                140000,
        "pg_stat_reset_shared('recovery_prefetch')":  140000,
        "pg_stat_reset_slru(null)":                   130000
      }\$json\$;

      -- Extension commands - a JSON object having the form:
      --                      {"command": "extension name"}
      reset_cmd_et json := \$json\${
        "pg_stat_statements_reset()":         "pg_stat_statements",
        "pg_stat_kcache_reset()":             "pg_stat_kcache",
        "pg_wait_sampling_reset_profile()":   "pg_wait_sampling",
        "pg_qualstats_reset()":               "pg_qualstats"
      }\$json\$;

      cmd record;

      cur_ver int;
    begin
      cur_ver := current_setting('server_version_num')::int;
      raise info 'Current PG version (num): %', cur_ver;

      -- Main reset commands
      for cmd in select * from json_each_text(reset_cmd_main) loop
        if cur_ver >= (cmd.value)::int then
          raise info 'Execute SQL: select %', cmd.key;  

          execute format ('select %s', cmd.key); 
        end if;
      end loop;

      -- Extension reset commands
      for cmd in select * from json_each_text(reset_cmd_et) loop
        if '' <> (
          select installed_version
          from pg_available_extensions
          where name = cmd.value
        ) then
          raise info 'Execute SQL: select %', cmd.key;  

          execute format ('select %s', cmd.key); 
        end if;
      end loop;
    end
    \$\$;
EOF
  then
    error "Failed to reset statistics before iteration."
  fi
}

function reset_pg_log() {
  info "Resetting Postgres log..."

  local pg_log_file=$(ls -t "${PG_LOG_DIR}" | grep .log | head -n 1)
  local pg_log_file_csv=$(ls -t "${PG_LOG_DIR}" | grep .csv | head -n 1)

  # Clear current log file
  if [[ -n "${pg_log_file}" ]]; then
    if ! echo '' > "${PG_LOG_DIR}/${pg_log_file}"; then
      error "Failed to clear the .log file."
    fi
  fi

  if [[ -n "${pg_log_file_csv}" ]]; then
    if ! echo '' > "${PG_LOG_DIR}/${pg_log_file_csv}"; then
      error "Failed to clear the .csv log file."
    fi
  fi

  # Delete other old log files
  find "${PG_LOG_DIR}" -type f ! -name "${pg_log_file}" ! -name "${pg_log_file_csv}" -delete
}

function start_sar_observation() {
  if [[ "${SOURCE_HOST}" == "localhost" ]]; then
    local sar_lib_file="/usr/lib/sysstat/sa1"
    local sar_log_file="/var/log/sysstat/sa$(date +%d)"
    local observation_interval=1

    info "Start SAR observation..."

    # Delete an existing sar data file
    if [[ -f "${sar_log_file}" ]]; then
      rm -f "${sar_log_file}" || error "Failed to remove existing sar data file: ${sar_log_file}"
    fi

    # Start sar data collection
    "${sar_lib_file}" -S XALL "${observation_interval}" &
    SAR_PID=$!

    # Check whether the process is running
    sleep 1  # Give a short time for the process to start
    if ! ps -p "${SAR_PID}" > /dev/null; then
      error "Failed to start sar observation."
    fi
  fi
}

function stop_sar_observation() {
  if [[ "${SOURCE_HOST}" == "localhost" ]]; then

    if ps -p "${SAR_PID}" > /dev/null 2>&1; then
      info "Stop sar observation..."

      # Try to complete the process correctly
      kill "${SAR_PID}" > /dev/null 2>&1
      # Give some time for the correct completion
      sleep 2
      # Check if the process has completed and, if not, force terminate
      if ps -p "${SAR_PID}" > /dev/null 2>&1; then
        kill -9 "${SAR_PID}" > /dev/null 2>&1
      fi
    fi
  fi
}

function start_pidstat_observation() {
  if [[ "${SOURCE_HOST}" == "localhost" ]]; then
    local observation_interval=10

    info "Start pidstat observation..."

    # recreate the temporary pidstat directory
    rm -rf /tmp/pidstat && mkdir -p /tmp/pidstat

    pidstat -lu "${observation_interval}" -p ALL > /tmp/pidstat/pidstat_cpu_usage.txt &
    CPU_PIDSTAT_PID=$!

    pidstat -lr "${observation_interval}" -p ALL > /tmp/pidstat/pidstat_memory_usage.txt &
    MEMORY_PIDSTAT_PID=$!

    pidstat -ld "${observation_interval}" -p ALL | grep -v loop > /tmp/pidstat/pidstat_disk_usage.txt &
    DISK_PIDSTAT_PID=$!
  fi
}

function stop_pidstat_observation() {
  if [[ "${SOURCE_HOST}" == "localhost" ]]; then
    local pids=("${CPU_PIDSTAT_PID}" "${MEMORY_PIDSTAT_PID}" "${DISK_PIDSTAT_PID}")
    local message_shown=false  # Initialize the flag for info message

    for pid in "${pids[@]}"; do
      if ps -p "${pid}" > /dev/null 2>&1; then

        if [[ "${message_shown}" != true ]]; then
          info "Stop pidstat observation..."
          message_shown=true
        fi

        # Try to complete the process correctly
        kill "${pid}" > /dev/null 2>&1
        # Give some time for the correct completion
        sleep 2
        # Check if the process has completed and, if not, force terminate
        if ps -p "${pid}" > /dev/null 2>&1; then
          kill -9 "${pid}" > /dev/null 2>&1
        fi
      fi
    done
  fi
}

function start_perf_observation() {
  if [[ "${SOURCE_HOST}" == "localhost" ]]; then
    local perf_observation_interval=10
    local perf_observation_pause=60 # Pause (1min) before the next iteration

    info "Start perf observation..."

    # recreate the temporary flamegraph directory
    rm -rf /tmp/flamegraph && mkdir -p /tmp/flamegraph

    # Collect perf data for all Postgres processes and Generate FlameGraph
    nohup bash -c "
      while true; do
        rm -rf perf.data
        perf record -F 99 -a -g -p \$(pgrep -d',' postgres) -- sleep ${perf_observation_interval}

        perf script -i perf.data \
          | /var/opt/FlameGraph/stackcollapse-perf.pl \
          | /var/opt/FlameGraph/flamegraph.pl \
          > /tmp/flamegraph/flamegraph_postgres.\$(date +%Y%m%d%H%M).svg

        sleep ${perf_observation_pause}
      done
    " >/dev/null 2>&1 &
    PERF_SCRIPT_PID=$!
  fi
}

function stop_perf_observation() {
  if [[ "${SOURCE_HOST}" == "localhost" ]]; then

    if ps -p "${PERF_SCRIPT_PID}" > /dev/null 2>&1; then
      info "Stop perf observation..."

      # Try to complete the process correctly
      kill "${PERF_SCRIPT_PID}" > /dev/null 2>&1
      # Give some time for the correct completion
      sleep 2
      # Check if the process has completed and, if not, force terminate
      if ps -p "${PERF_SCRIPT_PID}" > /dev/null 2>&1; then
        kill -9 "${PERF_SCRIPT_PID}" > /dev/null 2>&1
      fi
    fi
  fi
}

function collect_artifacts() {
  local label=$1
  local run_time=$(date +%Y-%m-%d-%H%M)
  local run_dir="${run_time}_${label}" # includes run_time and label

  mkdir -p "${ARTIFACTS_DIR}"/"${run_dir}" || info "[soft error, continue] Failed to create artifacts directory."

  # system artifacts:
  if [[ "${SOURCE_HOST}" == "localhost" ]]; then
    info "Collect system artifacts..."

    uname -a > "${ARTIFACTS_DIR}"/"${run_dir}"/uname-a.info
    lscpu > "${ARTIFACTS_DIR}"/"${run_dir}"/lscpu.info
    cat /proc/meminfo > "${ARTIFACTS_DIR}"/"${run_dir}"/meminfo.info
    lsblk > "${ARTIFACTS_DIR}"/"${run_dir}"/lsblk.info
    df -h > "${ARTIFACTS_DIR}"/"${run_dir}"/df-h.info
    sysctl -a > "${ARTIFACTS_DIR}"/"${run_dir}"/sysctl-a.info
    sysctl -p > "${ARTIFACTS_DIR}"/"${run_dir}"/sysctl-p.info
    cat /sys/kernel/mm/transparent_hugepage/enabled > "${ARTIFACTS_DIR}"/"${run_dir}"/transparent_hugepage.info
    ps -auxf > "${ARTIFACTS_DIR}"/"${run_dir}"/ps-auxf.info

    # copy sar artifacts
    local sar_log_file="/var/log/sysstat/sa$(date +%d)"
    if [[ -f "${sar_log_file}" ]]; then
      sar -u -f "${sar_log_file}" > "${ARTIFACTS_DIR}"/"${run_dir}"/sar_cpu_usage.txt
      sar -u -P ALL -f "${sar_log_file}" > "${ARTIFACTS_DIR}"/"${run_dir}"/sar_all_cpu_usage.txt
      sar -r -f "${sar_log_file}" > "${ARTIFACTS_DIR}"/"${run_dir}"/sar_memory_usage.txt
      sar -d -f "${sar_log_file}" | grep -v loop > "${ARTIFACTS_DIR}"/"${run_dir}"/sar_disk_usage.txt
      sar -n DEV -f "${sar_log_file}" > "${ARTIFACTS_DIR}"/"${run_dir}"/sar_network_usage.txt
      # all sar log file
      mv "${sar_log_file}" "${ARTIFACTS_DIR}"/"${run_dir}"/
    fi

    # copy pidstat artifacts
    if [[ -d "/tmp/pidstat" && $(ls -A /tmp/pidstat) ]]; then
      mv /tmp/pidstat/* "${ARTIFACTS_DIR}"/"${run_dir}"/
    fi

    # copy flamegraph artifacts
    if [[ -d "/tmp/flamegraph" && $(ls -A /tmp/flamegraph) ]]; then
      mv /tmp/flamegraph/* "${ARTIFACTS_DIR}"/"${run_dir}"/
    fi
  fi

  # PgBouncer
  if [[ "${SOURCE_HOST}" == "localhost" ]] && command -v pgbouncer &> /dev/null; then
    pgbouncer --version | head -n 1 > "${ARTIFACTS_DIR}"/"${run_dir}"/pgbouncer.version
  fi

  # Postgres artifacts:
  info "Collect Postgres artifacts..."

  # pg_stat_*** views
  local pg_stat_views=$(_PSQL -tAXc "select relname from pg_catalog.pg_class where relkind = 'view' and relname like 'pg_stat%'")
  [[ $? -ne 0 ]] && info "[soft error, continue] Failed to retrieve pg_stat views."

  for pg_stat_view in ${pg_stat_views}; do
    if ! _PSQL -Xc "copy (select * from ${pg_stat_view}) to stdout with csv header delimiter ',';" \
      > "${ARTIFACTS_DIR}"/"${run_dir}"/"${pg_stat_view}".csv; then
      info "[soft error, continue] Failed to collect ${pg_stat_view} data."
    fi
  done

  # pg_stat_kcache()
  if ! _PSQL -Xc "copy (select * from pg_stat_kcache()) to stdout with csv header delimiter ',';" \
    > "${ARTIFACTS_DIR}"/"${run_dir}"/pg_stat_kcache.csv; then
    info "[soft error, continue] Failed to collect pg_stat_kcache data."
  fi

  # pg_wait_sampling_profile
   if ! _PSQL -Xc "copy (
    select event_type as wait_type, event as wait_event, sum(count) as of_events from pg_wait_sampling_profile
    group by event_type, event order by of_events desc
    ) to stdout with csv header delimiter ',';" > "${ARTIFACTS_DIR}"/"${run_dir}"/pg_wait_sampling_profile.csv; then
    info "[soft error, continue] Failed to collect pg_wait_sampling_profile data."
  fi

  # pg_wait_sampling_profile_query
   if ! _PSQL -Xc "copy (
    select p.event_type as wait_type, p.event as wait_event, sum(p.count) as of_events, s.queryid, s.query
    from pg_wait_sampling_profile p join pg_stat_statements s on p.queryid = s.queryid
    group by s.queryid, s.query, p.event_type, p.event order by of_events desc
    ) to stdout with csv header delimiter ',';" > "${ARTIFACTS_DIR}"/"${run_dir}"/pg_wait_sampling_profile_query.csv; then
    info "[soft error, continue] Failed to collect pg_wait_sampling_profile_query data."
  fi

  # pg_settings
  if ! _PSQL -Xc "copy (select * from pg_settings order by name) to stdout with csv header delimiter ',';" \
    > "${ARTIFACTS_DIR}"/"${run_dir}"/pg_settings_all.csv; then
    info "[soft error, continue] Failed to collect pg_settings data."
  fi

  if ! _PSQL -Xc "copy (select name, setting as current_setting, unit, boot_val as default, context
    from pg_settings where source <> 'default') to stdout with csv header delimiter ','" \
    > "${ARTIFACTS_DIR}"/"${run_dir}"/pg_settings_non_default.csv; then
    info "[soft error, continue] Failed to collect pg_settings (non_default) data."
  fi

  if [[ "${SOURCE_HOST}" == "localhost" ]]; then
    # pg_conf
    local pg_conf_file=$(_PSQL -tAXc "show config_file")
    [[ -z "$pg_conf_file" ]] && info "[soft error, continue] Failed to retrieve PostgreSQL config file."

    cp "$pg_conf_file" "${ARTIFACTS_DIR}"/"${run_dir}"/ || info "[soft error, continue] Failed to copy PostgreSQL config file."

    # pg_log
    if [[ -n "${PG_LOG_DIR}" ]]; then
      for pg_log_file in $(ls "${PG_LOG_DIR}"); do

        case "${pg_log_file}" in
          *.csv) pg_log_file_name="postgresql_log.csv" ;;
          *.log) pg_log_file_name="postgresql.log" ;;
        esac

        if ! cp "${PG_LOG_DIR}/${pg_log_file}" "${ARTIFACTS_DIR}"/"${run_dir}"/"${pg_log_file_name}"; then
          info "[soft error, continue] Failed to copy \"${pg_log_file}\" to artifacts directory."
        fi
      done
    fi
  fi

  ARTIFACTS_COLLECTED=true
}

function check_postgres_readiness() {
  local is_ready=false

  for i in {1..60}; do
    local status=$(_PSQL -tAXc 'select pg_is_in_recovery()' 2> /dev/null)

    if [[ "$status" == "f" ]]; then
      is_ready=true
      info "Database is ready."
      break
    else
      info "Database is not ready yet."
      sleep 2
    fi
  done

  if [[ "$is_ready" != true ]]; then
    error "The database is not ready, the timeout has been exceeded."
  fi
}

function stop_postgres() {
  info "Stopping Postgres..."

  # Stopping the Patroni service
  if ! sudo systemctl stop patroni; then
    error "Failed to stop Patroni."
  fi
}

function start_postgres() {
  info "Starting Postgres..."

  # Starting the Patroni service
  if ! sudo systemctl start patroni; then
    error "Failed to start Patroni."
  fi

  # Checking Postgres availability
  check_postgres_readiness
}

function restart_postgres() {
  info "Restarting Postgres..."

  # Restarting the Patroni service
  if ! sudo systemctl restart patroni; then
    error "Failed to restart Patroni."
  fi

  # Checking Postgres availability
  check_postgres_readiness
}

function reload_postgres() {
  info "Reload Postgres configuration..."

  # Reload Postgres configuration
  if ! _PSQL -tAXc 'select pg_reload_conf()' &>/dev/null; then
    error "Failed to reload PostgreSQL configuration."
  fi

  # Check if a restart is required
  pending_restart_count=$(_PSQL -tAXc "select count(*) from pg_settings where pending_restart;")
    # restart Postgres if required (if the tests are running locally)
    if [[ "${SOURCE_HOST}" == "localhost" ]] && [[ "${pending_restart_count}" -gt 0 ]]; then
      restart_postgres
    # Stop, if restart is required (if the tests are not running locally)
    elif [[ "${SOURCE_HOST}" != "localhost" ]] && [[ "${pending_restart_count}" -gt 0 ]]; then
      error "Some configuration changes require a PostgreSQL restart."
    else
      info "Postgres configuration reloaded successfully (No restart is required)."
    fi
}

function clear_fs_cache() {
  stop_postgres

  info "Clearing file system cache..."

  # Ensure all buffered operations are written to disk
  if ! sync; then
    error "Error: Failed to synchronize disk writes."
  fi

  # Clear the file system cache
  if echo 3 > /proc/sys/vm/drop_caches; then
    info "File system cache cleared."
  else
    error "Error: Failed to clear file system cache."
  fi

  start_postgres
}

function pause_patroni() {
  if [[ "${SOURCE_HOST}" == "localhost" ]]; then
    patronictl pause --wait
  fi
}

function resume_patroni() {
  if [[ "${SOURCE_HOST}" == "localhost" ]]; then
    patronictl resume --wait
  fi
}

############## Pre-checks ###########################

# Ensure jq is installed
if ! command -v jq &> /dev/null; then
  error "jq not found."
fi

# Checking for the presence of an input argument
if [[ -z "$1" ]]; then
  error "No input provided."
fi

# Processing JSON input for test iterations
json_input=$(echo $1 | base64 -d 2> /dev/null || echo "$1") # Attempt to decode input data from Base64 or use simple JSON

# Validate JSON input and exit if it's not valid
if ! echo "${json_input}" | jq empty; then
  error "Invalid JSON input."
fi

# Supported types of operations
supported_types=("pre_shell" "pre_configs" "pre_sql" "workload_pgbench" "workload_sql")

# Checking the operation types from JSON input
operation_types=$(echo "${json_input}" | jq -r '.[] | keys[]' | sort -u)

for type in ${operation_types}; do
  if [[ ! "${supported_types[*]}" =~ "${type}" ]]; then
    # Create a string with a list of supported types, wrapped in quotation marks and separated by commas
    quoted_types=""
    for t in "${supported_types[@]}"; do
      if [[ -z "${quoted_types}" ]]; then
        quoted_types+="\"$t\""
      else
        quoted_types+=", \"$t\""
      fi
    done

    error "Unsupported operation type: \"${type}\". Supported types are: ${quoted_types}."
  fi
done

# Checking if 'pre_config' is an array
configs_string=$(echo "${json_input}" | jq '.. | .pre_configs? | select(type == "string")')
if [[ -n "${configs_string}" ]]; then
  error "'pre_configs' should be an array."
fi

# Extract 'pre_configs' as an array or null if it's not an array
configs_array=$(echo "${json_input}" | jq '.. | .pre_configs? | select(type == "array")')

# Check if 'pre_configs' exists and is an array
if [[ -n "${configs_array}" ]]; then
  # Convert JSON array to bash array
  readarray -t configs < <(echo "${configs_array}" | jq -r '.[]')

  # Check the format of each element in the 'pre_configs' array
  for config in "${configs[@]}"; do
    # Remove spaces (if any)
    config=$(echo "${config}" | sed -E 's/^[[:space:]]+|[[:space:]]+$//g; s/[[:space:]]*=[[:space:]]*/=/')

    # Extract the parameter name and value
    param_name=$(echo "${config}" | cut -d'=' -f1)
    param_value=$(echo "${config}" | cut -d'=' -f2-)

    # Remove surrounding quotes from param_value, if present
    param_value=$(echo "${param_value}" | sed -E "s/^['\"](.*)['\"]$/\1/")

    # Check if param_value is not empty
    if [[ -z "${param_value}" ]]; then
      error "Empty value for parameter '${param_name}' in 'pre_configs'. Empty values or values only containing quotes are not allowed."
    fi

    # Check if format is correct (key=value)
    if ! [[ "${config}" =~ ^[^=]+=.+$ ]]; then
      error "Invalid format in 'pre_configs': \"${config}\". Expected format: \"option=value\""
    fi
  done
fi

# Check 'workload_pgbench' format
workload=$(echo "${json_input}" | jq -r '.. | .workload_pgbench? | select(type == "string")')
if [[ -n "${workload}" && ! "${workload}" =~ pgbench ]]; then
  error "Invalid format in workload_pgbench: \"${workload}\". 'workload_pgbench' must include 'pgbench' call."
fi

# Exit the script if CHECK_ONLY=true is specified.
if [[ "$CHECK_ONLY" == "true" ]]; then
  echo "JSON input is valid." && exit 0
fi

############## Prepare ##############################

# Define the Postgres log directory
if [[ -z "${PG_LOG_DIR}" ]]; then
  PG_LOG_DIR=$(_PSQL -tAXc "show log_directory" 2>/dev/null)
fi

# Create extensions for all databases
databases=$(_PSQL -tAXc "select datname from pg_catalog.pg_database where datname <> 'template0'")
[[ $? -ne 0 ]] && error "Failed to retrieve list of databases."

for db in $databases; do
  for ext in pg_stat_statements pg_stat_kcache pg_wait_sampling; do
    if ! psql -h "${SOURCE_HOST}" -p "${SOURCE_PORT}" -U "${SOURCE_USER}" -d "${db}" \
      -tAXc "create extension if not exists ${ext}" &>/dev/null; then
      info "[soft error, continue] Failed to create extension ${ext} in database ${db}."
    fi
  done
done

# pgbench init
if [[ -n "${PGBENCH_INIT_COMMAND}" ]]; then
  info "Generating data with command: ${PGBENCH_INIT_COMMAND}"
  if ! eval "${PGBENCH_INIT_COMMAND}" | prefixes; then
    error "Failed to pgbench init."
  fi
fi

############## Run tests ############################

# Initialize a variable to track the current test label
current_label=""

# Iterate over each test iteration in the JSON
echo "${json_input}" | jq -r 'to_entries | .[] | .key as $k | .value | to_entries | map("\($k)=\(.key)=\(.value)") | .[]' \
  | while IFS='=' read -r label type cmd; do

  # Replace spaces in label with underscores or another safe character for filenames
  safe_label=$(echo "${label}" | sed 's/[^a-zA-Z0-9_-]/_/g')

  # Check if this is a new label
  if [[ "${label}" != "${current_label}" ]]; then
    info "RUN TEST: \"${label}\"" | tee -a "${ARTIFACTS_DIR}/${safe_label}_result.log"
    current_label="${label}"
  fi

  # pre_shell
  if [[ "${type}" == "pre_shell" ]]; then
    info "Executing ${type} command: $cmd" | tee -a "${ARTIFACTS_DIR}/${safe_label}_result.log"
    if ! eval "$cmd" 2>&1 | prefixes | tee -a "${ARTIFACTS_DIR}/${safe_label}_result.log"; then
      error "Failed to execute ${type} command." | tee -a "${ARTIFACTS_DIR}/${safe_label}_result.log"
    fi

  # pre_configs
  elif [[ "${type}" == "pre_configs" ]]; then
    # Parse the JSON array of config commands
    configs=$(echo "$cmd" | jq -r '.[]')
    if [[ $? -ne 0 ]]; then
      error "Failed to parse JSON commands."
    fi

    # Execute commands only if the array is not empty
    if [[ -n "${configs}" ]]; then
      echo "${configs}" | while read config; do

        # Remove spaces (if any)
        config=$(echo "${config}" | sed -E 's/^[[:space:]]+|[[:space:]]+$//g; s/[[:space:]]*=[[:space:]]*/=/')

        # Extract the parameter name and value
        param_name=$(echo "${config}" | cut -d'=' -f1)
        param_value=$(echo "${config}" | cut -d'=' -f2-)

        if [[ -z "${param_name}" || -z "${param_value}" ]]; then
          error "Invalid configuration command: ${config}."
        fi

        # Check if the value is not already wrapped in single or double quotes
        if ! [[ "${param_value}" =~ ^[\'\"].*[\'\"]$ ]]; then
          # Add quotes (excluding shared_preload_libraries option to support a list of values)
          if [[ "${param_name}" != "shared_preload_libraries" ]]; then
            param_value="'${param_value}'"
          fi
        fi

        # Now use the possibly updated ${param_value} to set the configuration
        info "Setting \"${param_name}\" configuration with command: ALTER SYSTEM SET ${param_name}=${param_value};" \
          | tee -a "${ARTIFACTS_DIR}/${safe_label}_result.log"

        if ! _PSQL -Xc "ALTER SYSTEM SET ${param_name}=${param_value};" 2>&1 | prefixes \
          | tee -a "${ARTIFACTS_DIR}/${safe_label}_result.log"; then
            error "Failed to set configuration." | tee -a "${ARTIFACTS_DIR}/${safe_label}_result.log"
        fi
      done

      # Reload Postgres configuration
      reload_postgres

      # Show the value of the applied parameters
      echo "${configs}" | while read config; do

        # Remove spaces (if any)
        config=$(echo "${config}" | sed -E 's/^[[:space:]]+|[[:space:]]+$//g; s/[[:space:]]*=[[:space:]]*/=/')

        # Extract the parameter name
        param_name=$(echo "$config" | cut -d'=' -f1)

        info "Show \"${param_name}\" configuration" | tee -a "${ARTIFACTS_DIR}/${safe_label}_result.log"
        if ! _PSQL -Xc "SHOW ${param_name};" 2>&1 | prefixes | tee -a "${ARTIFACTS_DIR}/${safe_label}_result.log"; then
          error "Failed to show configuration." | tee -a "${ARTIFACTS_DIR}/${safe_label}_result.log"
        fi
      done
    fi

  # pre_sql
  elif [[ "${type}" == "pre_sql" ]]; then

    function execute_pre_sql() {
      local sql_command=$1
      # Converting to single-line format
      sql_command=$(echo -e "${sql_command}" | tr '\n' ' ')

      # Workaround for CI pipelines where the $$ symbol is not properly escaped
      if [[ "${sql_command}" == *'$'* ]] && [[ "${sql_command}" != *'$$'* ]]; then
        sql_command=$(echo -e "${sql_command}" | sed 's/\$/\$\$/g')
      fi

      info "Executing ${type} with command: ${sql_command}" | tee -a "${ARTIFACTS_DIR}/${safe_label}_result.log"
      if ! _PSQL -Xc "${sql_command}" 2>&1 | prefixes | tee -a "${ARTIFACTS_DIR}/${safe_label}_result.log"; then
        error "Failed to execute sql." | tee -a "${ARTIFACTS_DIR}/${safe_label}_result.log"
      fi
    }

    # Checking whether cmd is an array
    if echo "${cmd}" | jq empty >/dev/null 2>&1 && [[ $(echo "${cmd}" | jq -r 'type') == "array" ]]; then
      # Executing each SQL command from an array
      echo "${cmd}" | jq -r '.[]' | while read -r sql_command_line; do
        execute_pre_sql "${sql_command_line}"
      done
    else
      # cmd is a string, run as a single SQL command
      execute_pre_sql "${cmd}"
    fi

  # workload_pgbench
  elif [[ "${type}" == "workload_pgbench" ]]; then
    # Reset Postgres statistics
    reset_pg_stat

    # Reset Postgres log
    if [[ "${SOURCE_HOST}" == "localhost" ]]; then
      reset_pg_log
    fi

    # Clear the file system cache
    if [[ "${SOURCE_HOST}" == "localhost" ]] && [[ "${CLEAR_FS_CACHE}" == "true" ]]; then
      clear_fs_cache
    fi

    # Enable maintenance mode for Patroni cluster, to avoid restarting Postgres during a stress testing
    pause_patroni

    # Start observation
    start_sar_observation
    start_pidstat_observation
    start_perf_observation

    info "Executing ${type} with command: ${cmd}" | tee -a "${ARTIFACTS_DIR}/${safe_label}_result.log"
    if ! eval "${cmd}" 2>&1 | prefixes | tee -a "${ARTIFACTS_DIR}/${safe_label}_result.log"; then
      error "Failed to execute ${type} command." | tee -a "${ARTIFACTS_DIR}/${safe_label}_result.log"
    fi

    # Stop observation
    stop_sar_observation
    stop_pidstat_observation
    stop_perf_observation

    # Collect artifacts
    collect_artifacts "${safe_label}"

    # Diable maintenance mode for Patroni cluster
    resume_patroni

  # workload_sql
  elif [[ "${type}" == "workload_sql" ]]; then

    function execute_workload_sql() {
      local sql_command=$1
      # Converting to single-line format
      sql_command=$(echo -e "${sql_command}" | tr '\n' ' ')

      # Workaround for CI pipelines where the $$ symbol is not properly escaped
      if [[ "${sql_command}" == *'$'* ]] && [[ "${sql_command}" != *'$$'* ]]; then
        sql_command=$(echo -e "${sql_command}" | sed 's/\$/\$\$/g')
      fi

      info "Executing ${type} with command: ${sql_command}" | tee -a "${ARTIFACTS_DIR}/${safe_label}_result.log"
      if ! _PSQL -Xc '\timing' -c "${sql_command}" 2>&1 | prefixes | tee -a "${ARTIFACTS_DIR}/${safe_label}_result.log"; then
        error "Failed to execute ${type} command." | tee -a "${ARTIFACTS_DIR}/${safe_label}_result.log"
      fi
    }

    # Reset Postgres statistics
    reset_pg_stat

    # Reset Postgres log
    if [[ "${SOURCE_HOST}" == "localhost" ]]; then
      reset_pg_log
    fi

    # Clear the file system cache
    if [[ "${SOURCE_HOST}" == "localhost" ]] && [[ "${CLEAR_FS_CACHE}" == "true" ]]; then
      clear_fs_cache
    fi

    # Start observation
    start_sar_observation
    start_pidstat_observation
    start_perf_observation

    # Checking whether cmd is an array
    if echo "${cmd}" | jq empty >/dev/null 2>&1 && [[ $(echo "${cmd}" | jq -r 'type') == "array" ]]; then
      # Executing each SQL command from an array
      echo "${cmd}" | jq -r '.[]' | while read -r sql_command_line; do
        execute_workload_sql "${sql_command_line}"
      done
    else
      # cmd is a string, run as a single SQL command
      execute_workload_sql "$cmd"
    fi

    # Stop observation
    stop_sar_observation
    stop_pidstat_observation
    stop_perf_observation

    # Collect artifacts
    collect_artifacts "${safe_label}"
  fi

done

# archive a ARTIFACTS directory
tar -zcvf "${ARTIFACTS_DIR}".tar.gz "${ARTIFACTS_DIR}" &>/dev/null

echo "Test is completed"
exit
